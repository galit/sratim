var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
const bodyParser = require('body-parser')
var morgan = require('morgan');
const db = require('./db');
const cors = require('cors');
const config = require('config');
const fs = require('fs');
const logger = require('./logger');
const middlewares = require('./middlewares')

const nunjucks = require('nunjucks')


var chekRouter = require('./api/check/check.router');
const showRouter = require('./api/show/show.router');
const ticketRouter = require('./api/ticket/ticket.router');
const acquisitionRouter = require('./api/acquisition/acquisition.router');
const subscriptionRouter = require('./api/subscription/subscription.router');
const creditRouter = require('./api/credit/credit.router');
const printTicketsRouter = require('./api/print-tickets/print-tickets.router');
const adminRouter = require('./api/admin/admin.router');

var app = express();
app.use(cors());

app.use(bodyParser.json())
app.use(morgan('combined', {stream: fs.createWriteStream('./logs/access.log', {flags: 'a'})}))
app.use(morgan('dev'));
// app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/check', chekRouter);
app.use('/show', showRouter);
app.use('/ticket', ticketRouter);
app.use('/acquisition', acquisitionRouter);
app.use('/subscription', subscriptionRouter);
app.use('/credit', creditRouter);
app.use('/print-tickets', printTicketsRouter);
app.use('/admin',adminRouter);

app.post('/not-suppurted-browser', (req, res, next) => {
  logger.warn('not suppurted browser detected');
  res.send();
})

const qr = require('qr-image');
app.get('/qr.svg', (req, res, next) => {
  var svg_string = qr.imageSync('guyugy', { type: 'svg' });
  res.send(svg_string);

})

app.use('/nunge', (req, res, next) => {
  const html = nunjucks.render('./tamplates/print.html');
  res.send(html);
})


app.use(express.static(path.join(__dirname, 'client/dist')))
app.use(express.static(path.join(__dirname, 'static')))

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'client/dist/index.html'))
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  console.log(err);
  let message;
  if(err.message && err.status !== 500 && err.status){
    logger.warn(err)
    message = err.message;
  }else{
    logger.error(err.stack)
    message = "הריעה שגיאה";
  }
  res.status(err.status || 500);
  res.json({message: message});
});


module.exports = app;
