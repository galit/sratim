const winston = require('winston');
const {format} = require('logform');


const alignedWithColorsAndTime = format.combine(
    // format.colorize(),
    format.timestamp(),
    // format.align(),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  );

const logger = winston.createLogger({
    level: 'info',
    format: alignedWithColorsAndTime,
    transports: [
      //
      // - Write to all logs with level `info` and below to `combined.log` 
      // - Write all logs error (and below) to `error.log`.
      //
      new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
      new winston.transports.File({ filename: './logs/action.log' })
    ]
  });


  module.exports = logger;