const express = require('express');
const router = express.Router();
const ticketController = require('./ticket.controller');

router.get('/byShowId/:showId', ticketController.getTicketsByShowId)

module.exports = router;