const dbModels = require('../../db').models;
const moment = require('moment');
const logger = require('../../logger');

function getTicketsByShowId(req, res, next) {
    const showId = req.params.showId
    logger.info(`ticket// getTicketsByShowId, showId:${showId}`);
    dbModels.Ticket.findAll({
        where: {showId: showId}, 
        include: [
            {model: dbModels.Seat, as: 'seat'},
            {model: dbModels.Show, as: 'show'}
        ]
    })
        .then(tickets => {
            tickets.forEach((ticket, i, arr) => {
                tickets[i].dataValues['isTaken'] = ticket.subscriptionId != null;
                if (ticket.lastReserved) {
                    const lr = moment(ticket.lastReserved);
                    const duration = moment.duration(moment().diff(lr))
                    if (duration.asMinutes() < 17) {
                        tickets[i].dataValues['isTaken'] = true;
                    }
                }
                delete tickets[i].dataValues.acquisitionId;
            })
            logger.info(`ticket// senging tickets by showid, showId:${showId}`)
            res.send(tickets)
        })
        .catch(err => next(err))
}


module.exports = {
    getTicketsByShowId
}