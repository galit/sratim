const dbModels = require('../../db').models;
const nunjucks = require('nunjucks');
const logger = require('../../logger');
const emailService = require('../../email.service');
const smsService = require('../../sms.service');

function success(req, res, next) {
    const params = req.body;
    const subscriptionId  = parseInt(params.pdesc);
    const subscriptionSum = parseInt(params.sum);
    logger.info(`credit// success subscriptionId: ${subscriptionId}`)
    dbModels.Subscription.findOne({where: {id: subscriptionId}})
        .then(s => {
            if (s.isConfirmed) {
                logger.error(`credit// double payment detected subscriptionId: ${s.id}`)
            }
            if(s.type != 'subscription') {
                Promise.all([
                    emailService.sendConfirmation(s)
                        .then(val => logger.info(`credit// confiramtion mail sent to ${s.ownerMail}`))
                        .catch(err => logger.error(`credit// error sending confiramtion mail to ${s.ownerMail}`)),
                    smsService.sendConfirmation(s)
                        .then(val => logger.info(`credit// confirmation sms sent to ${s.ownerPhone}`))
                        .catch(err => logger.error(`credit// error sending confirmation sms to ${s.ownerPhone}`))
                ])
            }
        })
    dbModels.Subscription.update(
        {
            isConfirmed: true
            // ,
            // myId: params.myid,
            // creditNumber: params.ccno,
            // creditSum: params.sum,
            // creditTk: params.TranzilaTK

        }, 
        {where: {id: subscriptionId}})
        .then(val => {
            html = nunjucks.render('./tamplates/credit-success.html', {subscriptionId: subscriptionId});
            logger.info(`credit// sending success page`)
            res.send(html);
        })
        .catch(err => next(err))
    
}

function fail(req, res, next) {
    const params = req.body;
    const subscriptionId  = parseInt(params.pdesc);
    logger.warn(`credit// fail subscriptionId: ${subscriptionId}`)
    dbModels.Subscription.update({isFailed: true}, {where: {id: subscriptionId}})
        .then(val => {
            html = nunjucks.render('./tamplates/credit-fail.html', {subscriptionId: subscriptionId});
            logger.info(`credit// sendign fail page`)
            res.send(html);
        })
        .catch(err => next(err))
}

function notify(req, res, next){
    const params = req.body;
    const subscriptionId  = parseInt(params.pdesc);
    logger.info(`credit// notified subscriptionId: ${subscriptionId}`)
    const subscriptionSum = parseInt(params.sum);
    dbModels.Subscription.update({
        isNotified: true,
        myId: params.myid,
        creditNumber: params.ccno,
        creditSum: params.sum,
        creditTk: params.TranzilaTK
    }, {where: {id: subscriptionId}})
        .then(val => res.send())
        .catch(err => next(err))
}

module.exports = {
    success,
    fail,
    notify
}