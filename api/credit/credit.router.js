const express = require('express');
const router = express.Router();

const creditController = require('./credit.controller');


router.post('/success', creditController.success)
router.post('/fail', creditController.fail)
router.post('/notify', creditController.notify)

module.exports = router;