let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../app');
let should = chai.should();

chai.use(chaiHttp);

describe('/check', function() {
    it('should return working', function(done) {
      chai.request(server)
        .get('/check/check')
        .end((err, res) =>{
            res.should.have.status(200)
            res.text.should.contain('working')
            done()
        })
    });
  }); 
  