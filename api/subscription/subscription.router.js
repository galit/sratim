const express = require('express');
const router = express.Router();
const subscriptionController = require('./subscription.controller');

router.post('/subscription-login', subscriptionController.subscriptionLogin);
router.get('/:subscriptionId', subscriptionController.findById);




module.exports = router;