const dbModels = require('../../db').models;
const createError = require('http-errors');
const jwt = require('jsonwebtoken')
const config = require('config')
const logger = require('../../logger');

async function subscriptionLogin(req, res, next){
    try{
        const subscriptionOwnerId = req.body.ownerId;
        const subscriptionId = req.body.subscriptionId;
        logger.info(`subscription// subscripionLogin subscriptionId: ${subscriptionId}, ownerId: ${subscriptionOwnerId}`)
        // TODO: dont return all of the attributes
        const foundSubscription = await dbModels.Subscription.findOne({
            where: {
                ownerId: subscriptionOwnerId,
                id: subscriptionId
            },
            include: [{model: dbModels.Ticket, as: 'tickets'}]
        })
        if(foundSubscription){
            const subscriptionJwt = jwt.sign(foundSubscription.id, config.jwtSecret);
            foundSubscription.dataValues['jwtId']  = subscriptionJwt;
            foundSubscription.dataValues['usage'] = foundSubscription.tickets.length;
            delete foundSubscription.dataValues['ticekts'];
            logger.info(`subscription// sending subscripion subscriptionId: ${subscriptionId}, ownerId: ${subscriptionOwnerId}`)
            res.send(foundSubscription);
        }
        else{
            next(createError(404, 'פרטים לא תואמים, נסה שוב'))
        }
    }
    catch(err){
        next(err)
    }
}

function findById(req, res, next) {
    console.log(req.params.subscriptionId, 'this is the [arams')
    const subscriptionId = req.params.subscriptionId;
    dbModels.Subscription.findOne({where:{id: subscriptionId}})
        .then(subscription => res.send(subscription))
        .catch(err => next(err))
}

module.exports = {
    subscriptionLogin,
    findById
}