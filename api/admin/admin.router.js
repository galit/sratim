const express = require('express');
const router = express.Router()
const adminController = require('./admin.controller');
const middlewhares = require('../../middlewares');

router.get('/get-subscriptions', middlewhares.assertAdmin, adminController.getAllSubscriptions);
router.post('/add-subscription', middlewhares.assertAdmin, adminController.addSubscriptions);
router.get('/get-tickets', middlewhares.assertAdmin, adminController.getTickets);
router.post('/dismiss-ticket', middlewhares.assertAdmin, adminController.dismissTicket);
router.post('/login', adminController.login);
router.post('/speciel-price-pay', middlewhares.assertAdmin, adminController.specialPricePay);
router.post('/subscription-pay', middlewhares.assertAdmin, adminController.subscirptionPay);

module.exports = router;