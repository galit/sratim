const dbModels = require('../../db').models;
const db = require('../../db').sequelize;
const httpError = require('http-errors');
const op = require('sequelize').Op;
const config = require('config');
const jwt = require('jsonwebtoken');
const validateTickets = require('../acquisition/validate-tickets');
const logger = require('../../logger');
const smsService = require('../../sms.service');

function getAllSubscriptions(req, res, next) {
    logger.info(`admin// getAllSubscriptions`)
    dbModels.Subscription.findAll({
        where: {type: 'subscription'},
        include: [{model: dbModels.Ticket, as: 'tickets'}]
    })
    .then(subscriptions => {
        logger.info(`admin// senging all subscriptions`)
        subscriptions.forEach(s => {s.dataValues['usage'] =s.tickets ? s.tickets.length : 0; console.log(s)})
        res.send(subscriptions);
    })
}

async function addSubscriptions(req, res, next) {
    logger.info(`admin// addSubscription`)
    const newSubscriptions = req.body.subscriptions;
    const retr = []
    try{
        for (const newSubscription of newSubscriptions){
            const temp = await dbModels.Subscription.create({
                ownerName: newSubscription.ownerName,
                ownerPhone: newSubscription.ownerPhone,
                ownerMail: newSubscription.ownerMail,
                ownerId: newSubscription.ownerId,
                price: newSubscription.price,
                ticketAmount: newSubscription.ticketAmount,
                type: 'subscription',
                secret: 'temp'
            })
            retr.push(temp)
        }
        retr.forEach(subscription => {
            smsService.sendSubscriptionConfiramtion(subscription)
                .then(val => logger.info(`admin// subscription confirmation send to ${subscription.ownerPhone}`))
                .catch(err => logger.error(`admin// error sending subscription confirmation to ${subscription.ownerPhone}`))
        })
        logger.info(`admin// subscriptions added ids: ${retr.map(val => val.id)}`)
        res.send(retr)
    
    }catch(err) {
        next(err);
    }

}

function filterTicketFunc(ticket, searchString) {
    if(!searchString) {
        return true;
    }
    if(ticket.subscription) {
        if(ticket.subscription.id.toString().includes(searchString)){
            return true;
        }
        if(ticket.subscription.ownerName.includes(searchString)){
            return true;
        }
        if(ticket.subscription.ownerId){
            if(ticket.subscription.ownerId.includes(searchString)){
                return true;
            }
        }
    }
    return false;
}

function getTickets(req, res, next){
    logger.info(`admin// getTickets`)
    const searchString = req.query.searchString;
    const showId = req.query.showId;
    const onlyReserved = req.query.onlyReserved === '1' ? true : false;

    const queryOptions = {
        limit: 500,
        include: [
            {model: dbModels.Show, as: 'show'},
            {model: dbModels.Seat, as: 'seat'},
            {model: dbModels.Subscription, as: 'subscription'}
        ]
    }
    queryOptions['where'] = {}
    if(showId) {
        queryOptions['where']['showId'] = showId;
    }
    if(onlyReserved) {
        queryOptions['where']['subscriptionId'] = {[op.not]: null}
    }

    dbModels.Ticket.findAll(queryOptions)
        .then(val => {
            logger.info('admin// sending tickets')
            res.send(val.filter( val => filterTicketFunc(val, searchString)))
        })
        .catch(err => next(err))
}

function dismissTicket(req, res, next) {
    const ticketId = req.body.ticketId;
    logger.info(`admin// dismissTicket ticketId: ${ticketId}`)
    // todo: delete and use schema validation
    if(!ticketId) {
        return next(httpError(403, 'bad request'))
    }
    dbModels.Ticket.update({subscriptionId: null}, {where: {id: ticketId}})
        .then(val => {
            logger.info(`admin// ticket dismissed ticketId: ${ticketId}`)
            res.send(val)
        })
        .catch(err => next(err))
}

async function login(req, res, next) {
    try{
        const username = req.body.username;
        const password = req.body.password;
        logger.info(`admin// login username: ${username}`)
        const user = await dbModels.User.findOne({where: {username: username, password: password}})
        if (user) {
            const userJwt = jwt.sign(user.id, config.jwtSecret);
            logger.info(`admin// login success username: ${username}`)
            res.send({userJwt});
        }
        else{
            next(httpError(404, 'פרטים לא תואמים, נסה שוב'));
        }
    } catch(err) {
        next(err);
    }
}

async function specialPricePay(req, res, next) {
    let t;
    try{
        const specialPrice = req.body.specielPrice;
        const name = req.body.name;
        const phone = req.body.phone;
        const email = req.body.email;
        const ticketsIds = req.body.ticketsIds;
        logger.info(`admin// specielPricePay ticketIds: ${ticketsIds}, specielPrice: ${specialPrice}`)
        
        t = await db.transaction();
        console.log('this is the db', db);
        console.log('this is the transaction', t)

        const validation = await validateTickets(ticketsIds);
        if(!validation.isValid){
            return next(httpError(403,'tickets validation faild'));
        }

        const tickets = await dbModels.Ticket.findAll({
            where: {
                id: {[op.in]: ticketsIds},
            },
            include: [{model: dbModels.Show, as: 'show'}],
            transaction: t
        
        })
        
        const ticketsPrice = tickets.length * specialPrice;
        const subscirption = await dbModels.Subscription.create(
            {
                type: 'speciel-price',
                ownerName: name,
                ownerPhone: phone,
                ownerMail: email,
                price: ticketsPrice,
                ticketAmount: tickets.length,
                secret: 'guyshir'
            }
        )

        await dbModels.Ticket.update({subscriptionId: subscirption.id}, {
            where: {
                id: {[op.in]: ticketsIds}
            },
            transaction: t
        })
        

        await t.commit()
        logger.info(`admin// speciel price payment success  ticketIds: ${ticketsIds}, specielPrice: ${specialPrice}`)
        res.json({transaction: {id: subscirption.id, price: subscirption.price}})
    }
    catch(err){
        await t.rollback()
        next(err)
    }
}

async function subscirptionPay(req, res, next) {
    let t;
    try{
        t = await db.transaction();
        const subscriptionIdJwt = req.body.subscriptionIdJwt;
        const ticketsIds = req.body.ticketsIds;
        const subscriptionId = req.body.subscriptionId;

        logger.info(`admin// subscriptionPay subscriptionId: ${subscriptionId} ticketIds: ${ticketsIds}`)
    
        const numberOfTickets = ticketsIds.length;
        
        const subscirption = await dbModels.Subscription.findOne({
            where: {id: subscriptionId, type: 'subscription'},
            include: [{model: dbModels.Ticket, as: 'tickets'}]
        })
        if (!subscirption) {
            throw httpError(404, 'מנוי לא נמצא')
        }
        
        if(numberOfTickets > subscirption.ticketAmount - subscirption.tickets.length){
            throw httpError(403, 'יתרת מנוי לא מספיקה לרכישה')
        }

        
        const validation = await validateTickets(ticketsIds, false, subscirption.id);
        if(!validation.isValid){
            return next(403, new Error(validation.message));
        }
        
        await dbModels.Ticket.update({subscriptionId: subscirption.id}, {
            where: {
                id: {[op.in]: ticketsIds}
            },
            transaction: t
        })
    
        const temp = await dbModels.Subscription.update(
            {usage: subscirption.usage + numberOfTickets}, 
            {where: {id: subscirption.id}, transaction: t}
        ) //remove and check with join
        await t.commit()
        logger.info(`admin// subscription payment success subscriptionId: ${subscriptionId} ticketIds: ${ticketsIds}`)
        res.send()
    }
    catch(err){
        await t.rollback()
        next(err)
    }
}

module.exports = {
    getAllSubscriptions,
    addSubscriptions,
    getTickets,
    dismissTicket,
    login,
    specialPricePay,
    subscirptionPay
}