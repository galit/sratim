const dbModels = require('../../db').models;
const db = require('../../db').sequelize;
const jwt = require('jsonwebtoken');
const config = require('config');
const httpError = require('http-errors');
const op = require('sequelize').Op;
const validateAckuisition = require('./validate-acquisition');
const validateTickets = require('./validate-tickets');
const logger = require('../../logger');
const emailService = require('../../email.service');
const smsService = require('../../sms.service');


async function validateAndReserve(req, res, next) {
    try{     
        const ticketsIds = req.body.ticketsIds;
        const subscriptionIdJwt = req.body.subscriptionIdJwt;
        logger.info(`acquisition// validating tickets ticketsIds: ${ticketsIds}`);
        let subscriptionId;
        if (subscriptionIdJwt) {
            subscriptionId = jwt.verify(subscriptionIdJwt, config.jwtSecret);
        }
        const validation = await validateTickets(ticketsIds, true, subscriptionId);
        if(!validation.isValid) {
            return next(httpError(403,  validation.message));
        }
        await dbModels.Ticket.update({lastReserved: new Date()}, {where: {id: {[op.in]: ticketsIds}}})
        logger.info(`acquisition// ticets validation success ticketsIds: ${ticketsIds} subscriptionId: ${subscriptionId}`)
        res.send();
    } catch(err) { 
        next(err);
    }
}




async function subscirptionAcuisition(req, res, next) {
    let t;
    try{
        const newAcquisitions = req.body.acquisitions;

        const subscriptionIdJwt = req.body.subscriptionIdJwt;
        const ticketsIds = req.body.ticketsIds;
        const subscriptionId = jwt.verify(subscriptionIdJwt, config.jwtSecret);
        logger.info(`acquisition// subscriptionAcquisition ticketsIds: ${ticketsIds}`)
    
        const numberOfTickets = ticketsIds.length;
        
        const subscirption = await dbModels.Subscription.findOne({
            where: {id: subscriptionId},
            include: [{model: dbModels.Ticket, as: 'tickets'}]
        })
        t = await db.transaction();
        if(numberOfTickets > subscirption.ticketAmount - subscirption.tickets.length){
            throw httpError(403, 'יתרת מנוי לא מספיקה לרכישה')
        }

        
        const validation = await validateTickets(ticketsIds, false, subscriptionId);
        if(!validation.isValid){
            return next(403, new Error(validation.message));
        }
        
        await dbModels.Ticket.update({subscriptionId: subscirption.id}, {
            where: {
                id: {[op.in]: ticketsIds}
            },
            transaction: t
        })
    
        const temp = await dbModels.Subscription.update(
            {usage: subscirption.usage + numberOfTickets}, 
            {where: {id: subscirption.id}, transaction: t}
        ) //remove and check with join
        Promise.all([
            emailService.sendConfirmation(subscirption)
                .then(val => logger.info(`acquisition// confiramtion mail sent to ${subscirption.ownerMail}`))
                .catch(err => logger.error(`acquisition// error sending confiramtion mail to ${subscirption.ownerMail}`)),
            smsService.sendConfirmation(subscirption)
                .then(val => logger.info(`acquisition// confirmation sms sent to ${subscirption.ownerPhone}`))
                .catch(err => logger.error(`acquisition// error sending confirmation sms to ${subscirption.ownerPhone}`))
        ])
        await t.commit()
        logger.info(`acquisition// subscriptionAcquisition auccess ticketsIds: ${ticketsIds}, subscriptionId: ${subscriptionId}`)
        res.send()
    }
    catch(err){
        await t.rollback()
        next(err)
    }
}


async function creditAcuisition(req, res, next) {
    let t;
    try{
        const name = req.body.name;
        const phone = req.body.phone;
        const email = req.body.email;
        const ticketsIds = req.body.ticketsIds;

        logger.info(`acquisition// creditAcquisition ticketsIds: ${ticketsIds} email: ${email} phone: ${phone} mail: ${email}`)
        
        t = await db.transaction();

        const validation = await validateTickets(ticketsIds);
        if(!validation.isValid){
            return next(new Error('tickets validation faild'));
        }

        const tickets = await dbModels.Ticket.findAll({
            where: {
                id: {[op.in]: ticketsIds},
            },
            include: [{model: dbModels.Show, as: 'show'}],
        
        })
        
        const ticketsPrice = tickets.reduce((price, ticket) => price + ticket.show.price, 0);
        const subscirption = await dbModels.Subscription.create(
            {
                type: 'regular',
                ownerName: name,
                ownerPhone: phone,
                ownerMail: email,
                price: ticketsPrice,
                ticketAmount: tickets.length,
                secret: 'guyshir'
            }
        )

        await dbModels.Ticket.update({subscriptionId: subscirption.id}, {
            where: {
                id: {[op.in]: ticketsIds}
            },
            transaction: t
        })
        

        await t.commit()
        logger.info(`acquisition// creditAcquisition success subscriptionId: ${subscirption.id} ticketsIds: ${ticketsIds} email: ${email} phone: ${phone} mail: ${email}`)
        res.json({transaction: {id: subscirption.id, price: subscirption.price}})
    }
    catch(err){
        await t.rollback()
        next(err)
    }
}


module.exports = {
    subscirptionAcuisition,
    creditAcuisition,
    validateAndReserve
}