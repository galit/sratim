const dbModels = require('../../db').models;
const op = require('sequelize').Op;

async function validateAcquisition(acquisition) {
    
    ticketsIds = acquisition.tickets;
    
    const tickets = await dbModels.Ticket.findAll({
        where: {
            id: {[op.in]: ticketsIds},
        },
        include: [{model: dbModels.Show, as: 'show'}]
    })

    for(let ticket of tickets) {
        if(ticket.acquisitionId != null || ticket.showId != acquisition.showId){
            return false
        }
    }

    return true;
}

module.exports = validateAcquisition;