let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../../app');
let should = chai.should();

chai.use(chaiHttp);


describe('acqusition', () => {
    it('add new acquisiton to the db', (done) => {
        chai.request(server)
            .post('/acquisition')
            .send({
                acquisitions: [
                    {tickets: [6,7,8,9]},
                    {tickets: [2,3,4]}
                ]
            })
            .end((err, res) => {
                console.log(res.text);
                done()
            })
    })
})