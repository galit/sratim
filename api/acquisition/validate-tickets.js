const dbModels = require('../../db').models;
const op = require('sequelize').Op;
const moment = require('moment');


async function validateTickets(ticketsIds, validateLastReserved = false, subscriptionId = undefined) {
    try{
        console.log('starting validation', ticketsIds)
        const tickets = await dbModels.Ticket.findAll({
            where: {
                id: {[op.in]: ticketsIds}
            }
        })

        if (subscriptionId) {
            const subscriptionTicketsForShowes = await dbModels.Ticket.findAll({where: {
                subscriptionId: subscriptionId,
                showId: {[op.in]: tickets.map(val => val.showId)}
            }
        })
        console.log(subscriptionTicketsForShowes.map(val => val.id), 'this is the tickets');
        if(subscriptionTicketsForShowes.length) {
            console.log(subscriptionTicketsForShowes, 'this is this')
            return {isValid: false, message: 'מנוי הינו אישי, כבר בחרת כרטיס לסרט זה'}
        }

        }

        for(let ticket of tickets) {
            if(ticket.subscriptionId != null){
                console.log('checking', ticket.id)
                return {isValid: false, message: 'הכרטיסים כבר נבחרו'}
            }
            
            if(validateLastReserved) {
                if (ticket.lastReserved) {
                    const lr = moment(ticket.lastReserved);
                    const duration = moment.duration(moment().diff(lr))
                    if (duration.asMinutes() < 17) {
                        return {isValid: false, message: 'הכרטיסים כבר נבחרו'}
                    }
                }
            }
        }

        return {isValid: true};
    }catch(err){
        throw(err);
    }
}


module.exports = validateTickets;