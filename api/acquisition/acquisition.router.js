const express = require('express');
const router = express.Router();

const acquisitionController = require('./acquisition.controller')

router.post('/subscription', acquisitionController.subscirptionAcuisition)

router.post('/credit', acquisitionController.creditAcuisition)

router.post('/reserve', acquisitionController.validateAndReserve)

module.exports = router;