const express = require('express');
const router = express.Router();
const showController = require('./show.controler')

router.get('/', showController.getAllShows)

module.exports = router;