const dbModels = require('../../db').models
const logger = require('../../logger');

const getAllShows = (req, res, next) => {
    logger.info(`show// getAllShows`)
    dbModels.Show.findAll()
        .then(val => {
            logger.info(`sending all shows`)
            res.send(val);
        })
        .catch(err => next(err))
}

module.exports = {
    getAllShows
}