const nunjucks = require('nunjucks')

function getPrintPage(tickets) {
    return nunjucks.render('./tamplates/print.html', {tickets: tickets})
}

module.exports = getPrintPage;