const dbModels = require('../../db').models;
const getPrintPage = require('./get-print-page');
const op = require('sequelize').Op;
const logger = require('../../logger');


function byShowId(req, res, next) {
    const showId = req.params.showId
    dbModels.Ticket.findAll({
        where: {showId: showId}, 
        include: [
            {model: dbModels.Seat, as: 'seat'},
            {model: dbModels.Show, as: 'show'},
            {model: dbModels.Acquisition, as: 'acquisition', required: true}
        ]
    })
    .then(val => {
        const retr = getPrintPage(val);
        res.send(retr);
    })
    .catch(err => next(err))
}

async function byIds(req, res, next) {
    logger.info(`prin-tickets// byIds`)
    try{
        const ticketIds = req.body.ticketIds;
        const tickets = await dbModels.Ticket.findAll({
            where: {id: {[op.in]: ticketIds}},
            include: [
                {model: dbModels.Seat, as: 'seat'},
                {model: dbModels.Show, as: 'show'},
                {model: dbModels.Subscription, as: 'subscription'}
            ]
        })
        await dbModels.Ticket.update({isPrinted: true}, {where: {id: {[op.in]: ticketIds}}})
        const retr = getPrintPage(tickets);
        logger.info('print-tickets// sending print-page')
        res.send({pritnPage: retr});
    } catch(err) {
        next(err)
    }
}

module.exports = {
    byShowId,
    byIds
}