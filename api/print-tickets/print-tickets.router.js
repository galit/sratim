const express = require('express');
const router = express.Router();
const printTicketsController = require('./print-tickets.controller');

router.get('/byShowId/:showId', printTicketsController.byShowId)
router.post('/by-ids', printTicketsController.byIds)
module.exports =  router;