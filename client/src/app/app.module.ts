import { StayInPaymentGuard } from './core/services/stay-in-payment-guard';
import { AdminGuard } from './core/services/admin-guard';
import { FilterSubscriptionsPipe } from './pipes/filter-subscriptions';
import { GroupTicketsPipe } from './pipes/group-tickets';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Route } from '@angular/router';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { CoreModule } from './core/core.module';

import { HomeComponent } from './components/home/home.component';
import { ExamplePageComponent } from './components/example-page/example-page.component';
import { ChoseSeatComponent } from './components/chose-seat/chose-seat.component';
import { CartComponent } from './components/cart/cart.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { SubscriptionComponent } from './components/subscription/subscription.component';
import { SuccessComponent } from './components/success/success.component';
import { WellcomeGaurd } from './core/services';
import { SubscriptionsComponent } from './components/subscriptions/subscriptions.component';
import { CreateSubscriptionComponent } from './components/create-subscription/create-subscription.component';
import { TicketsManagementComponent } from './components/tickets-management/tickets-management.component';
import { LoginComponent } from './components/login/login.component';
import { PaymentComponent } from './components/payment/payment.component';
import { TakanonComponent } from './components/takanon/takanon.component';
import { ContactComponent } from './components/contact/contact.component';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { DeviceGuard } from './core/services/device-guard';
import { NotSuppurtedComponent } from './components/not-suppurted/not-suppurted.component';


const childredRoutes: Route[] = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'choose-seat'
  },

  {
    path: 'example',
    pathMatch: 'full',
    component: ExamplePageComponent
  },
  {
    path: 'choose-seat',
    component: ChoseSeatComponent,
    canActivate: [WellcomeGaurd]
  },
  {
    path: 'choose-seat/:showId',
    component: ChoseSeatComponent,
    canActivate: [WellcomeGaurd]
  },
  {
    path: 'cart',
    component: CartComponent
  },
  {
    path: 'wellcome',
    component: WelcomeComponent
  },
  {
    path: 'subscription',
    component: SubscriptionComponent
  },
  {
    path: 'success',
    component: SuccessComponent
  },
  {
    path: 'backoffice/subscriptions',
    component: SubscriptionsComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'backoffice/add-subscription',
    component: CreateSubscriptionComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'backoffice/tickets',
    component: TicketsManagementComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'backoffice/login',
    component: LoginComponent
  },
  {
    path: 'payment/:subscriptionId',
    component: PaymentComponent,
    canDeactivate: [StayInPaymentGuard]
  },
  {
    path: 'takanon',
    component: TakanonComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  }
];

const routes: Route[] = [{
  path: '',
  canActivate: [DeviceGuard],
  children: childredRoutes
},
{
  path: 'not-suppurted',
  component: NotSuppurtedComponent
}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ExamplePageComponent,
    ChoseSeatComponent,
    CartComponent,
    WelcomeComponent,
    SubscriptionComponent,
    SuccessComponent,
    GroupTicketsPipe,
    FilterSubscriptionsPipe,
    SubscriptionsComponent,
    CreateSubscriptionComponent,
    TicketsManagementComponent,
    LoginComponent,
    PaymentComponent,
    TakanonComponent,
    ContactComponent,
    NotSuppurtedComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    CoreModule,
    RouterModule.forRoot(routes, { enableTracing: false }),
    FormsModule,
    ReactiveFormsModule,
    DeviceDetectorModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
