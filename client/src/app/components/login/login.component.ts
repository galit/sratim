import { AdminService } from './../../core/services/admin.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ToastyHelperService } from '../../core/services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;

  constructor(private adminService: AdminService, private toastr: ToastyHelperService, private router: Router) { }

  login() {
    this.adminService.adminLogin(this.username, this.password).subscribe(
      val => {
        this.toastr.showSuccess('מחובר', '');
        this.router.navigateByUrl('/backoffice/tickets');
      },
      err => {
        this.toastr.showError('שגיאה', err.message);
        this.password = '';
      }
    );
  }

  ngOnInit() {
  }

}
