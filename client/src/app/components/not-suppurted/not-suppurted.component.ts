import { DeviceDetectorService } from 'ngx-device-detector';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-not-suppurted',
  templateUrl: './not-suppurted.component.html',
  styleUrls: ['./not-suppurted.component.css']
})
export class NotSuppurtedComponent implements OnInit {
  device: any;
  constructor(private deviceDetectorService: DeviceDetectorService) { }

  ngOnInit() {
    this.device = {
      browser: this.deviceDetectorService.browser,
      version: this.deviceDetectorService.browser_version
    };
  }

}
