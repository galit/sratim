import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotSuppurtedComponent } from './not-suppurted.component';

describe('NotSuppurtedComponent', () => {
  let component: NotSuppurtedComponent;
  let fixture: ComponentFixture<NotSuppurtedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotSuppurtedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotSuppurtedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
