import { environment } from './../../../environments/environment';
import { ActivatedRoute } from '@angular/router';
import { StoreService } from './../../core/services/store.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Subscription } from '../../core/models';
import { ToastyHelperService } from '../../core/services';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.css']
})
export class PaymentComponent implements OnInit {

  subscription: Subscription;
  tranzilaIframeUrl: any;
  isLoading = true;
  state = 'loading';

  constructor(
    private domSanitizer: DomSanitizer,
    private storeService: StoreService,
    private toastr: ToastyHelperService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe(val => {
      console.log('this is it', val.subscriptionId);
      this.getSubscription(val.subscriptionId);
    });
  }

  getSubscription(subscriptionId) {

    this.storeService.getSubscriptionById(subscriptionId)
      .subscribe(val => {
        this.subscription = val;
        console.log(this.subscription);
        if (!this.subscription) {
          this.state = 'error';
          return;
        }
        if (!this.subscription.price || !this.subscription.id) {
          this.state = 'error';
          return;
        }
        if (this.subscription.isConfirmed) {
          this.state = 'confirmed';
          return;
        }
        // tslint:disable-next-line:max-line-length
        this.tranzilaIframeUrl = this.domSanitizer.bypassSecurityTrustResourceUrl(`https://direct.tranzila.com/${environment.masof}/iframe.php?sum=${this.subscription.price}&currency=1&cred_type=1&lang=il&nologo=1&pdesc=${this.subscription.id}&trBgColor=e9ecef&tranmode=AK&email=${this.subscription.ownerMail}&contact=${this.subscription.ownerName}`);
        this.state = 'payment';
      }, err => this.state = 'error');
  }

}
