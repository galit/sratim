import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakanonComponent } from './takanon.component';

describe('TakanonComponent', () => {
  let component: TakanonComponent;
  let fixture: ComponentFixture<TakanonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakanonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakanonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
