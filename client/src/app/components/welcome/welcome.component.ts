import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {StoreService} from '../../core/services';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  nextUrl: string;

  constructor(private router: Router, private storeService: StoreService, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      val => this.nextUrl = val.return || '/choose-seat'
    );
  }

  navigateToSubscription() {
    this.storeService.isSubscriptionChecked = true;
    this.router.navigate(['/subscription'], {queryParams: {return: this.nextUrl}});
  }

  navigateToSeats() {
    this.storeService.isSubscriptionChecked = true;
    this.router.navigateByUrl(this.nextUrl);
  }

  navigate(url: string) {
    this.storeService.isSubscriptionChecked = true;
    this.router.navigateByUrl(url);
  }

}
