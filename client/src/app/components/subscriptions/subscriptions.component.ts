import { ToastyHelperService } from './../../core/services/toasty-helper.service';
import { Subscription } from './../../core/models';
import { AdminService } from './../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-subscriptions',
  templateUrl: './subscriptions.component.html',
  styleUrls: ['./subscriptions.component.css']
})
export class SubscriptionsComponent implements OnInit {
  subscriptions: Subscription[];
  filterByText: string;
  isLoading = false;

  constructor(
    private adminService: AdminService,
    private toastr: ToastyHelperService,
    private router: Router
  ) { }

  ngOnInit() {
    this.adminService.subscriptions.subscribe(
      val => this.subscriptions = val
    );

    this.isLoading = true;
    this.adminService.retriveSubscriptions().subscribe(
      val => {
        this.isLoading = false;
      },
      err => {
        this.isLoading = false;
        this.toastr.showError('שגיאה', 'שגיאה בטעינת עסקאות');
      }
    );
  }
  navigateToAddSubscription() {
    this.router.navigateByUrl('/backoffice/add-subscription');
  }

  chargeSubscription(subscription) {
    this.router.navigateByUrl(`/payment/${subscription.id}`);
  }

}
