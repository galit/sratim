import { AdminService } from './../../core/services/admin.service';
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import {ToastyHelperService, StoreService, ShowService} from '../../core/services';
import {Show, Ticket, Seat, Subscription} from '../../core/models';
import { Router, ActivatedRoute } from '@angular/router';
import {hebrawWeekDay, arrayToCuples} from '../../../utils';

// const getRowTickets = (tickets: Ticket[], startSeat: number, endSeat: number) => {
  // return tickets.filter(ticket => ticket.seat.id >= startSeat && ticket.seat.id <= endSeat);
// };

function getRowTickets(tickets: Ticket[], rowNumber: number, sectionNumber: number) {
  return tickets
    .filter(ticket => ticket.seat.row === rowNumber && ticket.seat.section === sectionNumber)
    .sort((b, a) => a.seat.seatId - b.seat.seatId);
}


@Component({
  selector: 'app-chose-seat',
  templateUrl: './chose-seat.component.html',
  styleUrls: ['./chose-seat.component.css']
})
export class ChoseSeatComponent implements OnInit {
  displayType = 'loader';
  payMode: string;
  subscription: Subscription;
  seatDetails = {};
  tickets: Ticket[] = [];
  rows: any = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}};
  hebrawWeekDay = hebrawWeekDay;
  isAdmin: boolean;
  selectedTickets: Ticket[] = [];

  shows: Show[] = [{name: 'בחר סרט'} as Show];
  selectedShow: Show = this.shows[0];

  @ViewChild('theather') theaterDiv: ElementRef;

  constructor(
    private toastr: ToastyHelperService,
    private router: Router,
    private route: ActivatedRoute,
    private storeService: StoreService,
    private showService: ShowService,
    private adminService: AdminService,
    private ref: ChangeDetectorRef
  ) {

   }

  splitTicketsToRows() {
    // firstRow
    this.rows[1][1] = getRowTickets(this.tickets, 1, 1);

    // secondRow

    this.rows[2][1] = arrayToCuples(getRowTickets(this.tickets, 2, 1));
    this.rows[2][2] = arrayToCuples(getRowTickets(this.tickets, 2, 2));
    // left section
    this.rows[3][1] = getRowTickets(this.tickets, 3, 1);
    this.rows[4][1] = getRowTickets(this.tickets, 4, 1);
    this.rows[5][1] = getRowTickets(this.tickets, 5, 1);
    this.rows[6][1] = getRowTickets(this.tickets, 6, 1);
    this.rows[7][1] = getRowTickets(this.tickets, 7, 1);
    this.rows[8][1] = getRowTickets(this.tickets, 8, 1);
    this.rows[9][1] = getRowTickets(this.tickets, 9, 1);

    // right section
    this.rows[3][2] = getRowTickets(this.tickets, 3, 2);
    this.rows[4][2] = getRowTickets(this.tickets, 4, 2);
    this.rows[5][2] = getRowTickets(this.tickets, 5, 2);
    this.rows[6][2] = getRowTickets(this.tickets, 6, 2);
    this.rows[7][2] = getRowTickets(this.tickets, 7, 2);
    this.rows[8][2] = getRowTickets(this.tickets, 8, 2);
    this.rows[9][2] = getRowTickets(this.tickets, 9, 2);
  }

  ngOnInit() {
    this.storeService.ticketsChange.subscribe(val => this.selectedTickets = val);
    this.showService.getShows()
      .subscribe(
        val => {
          this.shows = this.shows.concat(val);
          this.displayType = 'feather';
          if (this.route.snapshot.params.showId && !isNaN(this.route.snapshot.params.showId)) {
            const rshowId  = parseInt(this.route.snapshot.params.showId, 10);
            console.log(rshowId, 'this is the show id', this.shows);
            const rshow = this.shows.find(show => show.id === rshowId);
            console.log('this is the show', rshow);
            this.selectedShow = rshow;
            this.onShowSelect();
          }

        },
        err => {
          this.toastr.showError('שגיאה בטעינת סרטים', '');
          this.displayType = 'feather';
        }
      );
    this.adminService.isAdmin.subscribe(val => this.isAdmin = val);
    this.storeService.mode.subscribe(val => this.payMode = val);
    this.storeService.subscription.subscribe(val => this.subscription = val);
  }

  triggerChange() {
    this.ref.detectChanges();
  }
  scrollRight() {
    console.log(this.theaterDiv);
    this.theaterDiv.nativeElement.scrollLeft += 40;
    console.log(this.theaterDiv.nativeElement.scrollLeft);
  }

  scrollLeft() {
    console.log(this.theaterDiv);
    this.theaterDiv.nativeElement.scrollLeft -= 40;
    console.log(this.theaterDiv.nativeElement.scrollLeft);
  }

  canScrollRight() {
    return this.theaterDiv.nativeElement.scrollLeft < this.theaterDiv.nativeElement.scrollWidth - this.theaterDiv.nativeElement.offsetWidth;
  }

  canScrollLeft() {
    return this.theaterDiv.nativeElement.scrollLeft > 0;
  }

  onShowSelect() {
    this.router.navigateByUrl('/choose-seat/' + this.selectedShow.id);
    this.displayType = 'loader';
    this.showService.getShowTickets(this.selectedShow.id)
      .subscribe(val => {
        this.tickets = val;
        this.splitTicketsToRows();
        this.displayType = 'feather';
      }, err => {
        this.displayType = 'feather';
        this.toastr.showError('שגיאה בטעינת כרטיסים', '');
        this.clearSelections();
      });
  }

  clearSelections() {
    this.selectedShow = this.shows[0];
    this.tickets = [];
    this.rows = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}, 6: {}, 7: {}, 8: {}, 9: {}};
  }

  findTicket(section: number, row: number, seatId: number) {
    const foundTicket = this.tickets.find(ticket =>
      ticket.seat.section === section && ticket.seat.row === row && ticket.seat.seatId === seatId
    );
    return foundTicket ? foundTicket : false;
  }

  checkForLoneSeat(ticket: Ticket): boolean {
    const section = ticket.seat.section;
    const row = ticket.seat.row;
    const seatId = ticket.seat.seatId;
    const ticketToRight = this.findTicket(section, row, seatId + 1);
    const secondTicketToRight = this.findTicket(section, row, seatId + 2);
    const ticketToLeft = this.findTicket(section, row, seatId - 1);
    const secondTicketToLeft = this.findTicket(section, row, seatId - 2);
    if (ticketToRight) {
      if (!ticketToRight.isChosen && !ticketToRight.isTaken) {
        if (secondTicketToRight) {
          if (secondTicketToRight.isChosen || secondTicketToRight.isTaken) {
            return true;
          }
        } else {
          return true;
        }
      }
    }
    if (ticketToLeft) {
      if (!ticketToLeft.isChosen && !ticketToLeft.isTaken) {
        if (secondTicketToLeft) {
          if (secondTicketToLeft.isChosen || secondTicketToLeft.isTaken) {
            return true;
          }
        } else {
          return true;
        }
      }
    }
  }

  onSelectTicketsClick() {
    const selectedTickets = this.tickets.filter(ticket => ticket.isChosen);
    if (selectedTickets.length <= 0) {
      this.toastr.showWarning('', 'לא נבחרו כרטיסים');
      return;
    }
    if (!this.isAdmin) {
      if (selectedTickets.length > 10) {
        this.toastr.showWarning('', 'אין לבחור יותר מ10 כרטיסים לסרט יחיד');
        return;
      }

      if (this.payMode === 'subscription' && selectedTickets.length > 1) {
        this.toastr.showWarning('מנוי הינו אישי', 'אין אפשרות לבחור יותר מכרטיס אחד עבור סרט');
        return;
      }

      if (this.payMode === 'subscription') {
        console.log(this.selectedTickets, 'this is the seleted tickets');
        for (const ticket of selectedTickets) {
          console.log(this.selectedTickets.map(val => val.show.id), ticket.show.id, 'this is the stuf');
          if (this.selectedTickets.map(val => val.show.id).includes(ticket.show.id)) {
            this.toastr.showWarning('מנוי הינו אישי', 'כבר בחרת כרטיס לסרט זה');
            return;
          }
        }
      }

      for (const ticket of selectedTickets) {
        if (this.checkForLoneSeat(ticket)) {
          this.toastr.showWarning('', 'אין להשאיר כסא בודד');
          return;
        }
      }
    }


    this.displayType = 'loader';
    this.showService.checkIfTicketsValid(selectedTickets)
      .subscribe(val => {
        this.storeService.addTickets(selectedTickets);
        this.clearSelections();
        this.displayType = 'success';
      },
    err => {
      this.displayType = 'feather';
      this.toastr.showWarning('שגיאה בבחירת כרטיס', err.message);
    });

  }

  cencelSelections() {
    this.tickets.forEach(t => t.isChosen = false);
  }

  onSeatClick(ticket: Ticket) {
    if (ticket.isTaken) {
      this.toastr.showWarning('', 'הכיסא תפוס');
      return null;
    }
    if (!ticket.isChosen) {
      if (this.subscription) {
        if (this.subscription.ticketAmount === this.subscription.usage) {
          this.toastr.showWarning('', 'לא נותרו כרטיסים במנוי');
          return ;
        }
        // this.subscription.usage += 1;
      }
      ticket.isChosen = true;
    } else {
      if (this.subscription) {
        // this.subscription.usage -= 1;
      }
      ticket.isChosen = false;
    }

  }

  onChoseAnotherTicketClick() {
    this.displayType = 'feather';
  }

  navigateToCart() {
    this.router.navigateByUrl('/cart');
  }

}
