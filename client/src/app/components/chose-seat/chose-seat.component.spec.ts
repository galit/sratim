import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoseSeatComponent } from './chose-seat.component';

describe('ChoseSeatComponent', () => {
  let component: ChoseSeatComponent;
  let fixture: ComponentFixture<ChoseSeatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoseSeatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoseSeatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
