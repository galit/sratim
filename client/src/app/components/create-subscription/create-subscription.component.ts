import { Router } from '@angular/router';
import { Subscription } from './../../core/models/subscription.model';
import { ToastyHelperService } from './../../core/services/toasty-helper.service';
import { AdminService } from './../../core/services/admin.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-create-subscription',
  templateUrl: './create-subscription.component.html',
  styleUrls: ['./create-subscription.component.css']
})
export class CreateSubscriptionComponent implements OnInit {

  displayType = 'form';
  newSubscriptions: Subscription[];
  subscriptions: Subscription[] = [];
  isLoading = false;
  subscriptionTypes = {
    'בחר סוג מנוי...': {price: null, ticketAmount: null},
    'מנוי 10': {price: 350, ticketAmount: 10},
    'מנוי 5': {price: 250, ticketAmount: 5},
    'חבילת לינה': {price: 0, ticketAmount: 9}
  };
  subscriptionTypesNames = Object.keys(this.subscriptionTypes);

  subscriptionForm = new FormGroup({
    ownerName: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
    ownerMail: new FormControl('', [Validators.required, Validators.email]),
    ownerPhone: new FormControl('', [Validators.required, Validators.pattern('0[0123456789]+'), Validators.maxLength(15)]),
    ownerId: new FormControl('', [Validators.required, Validators.pattern('[0123456789]+'), Validators.maxLength(15)]),
    // tslint:disable-next-line:max-line-length
    ticketAmount: new FormControl('', [Validators.required, Validators.pattern('[0123456789]+'), Validators.maxLength(2), Validators.min(0)]),
    price: new FormControl('', [Validators.required, Validators.pattern('[0123456789]+'), Validators.maxLength(15), Validators.min(0)])
  });

  constructor(
    private adminService: AdminService,
    private toastr: ToastyHelperService,
    private router: Router
  ) { }

  ngOnInit() {
  }
  onSubscriptionTypeChange(e) {
    console.log(this.subscriptionTypes[e.target.value]);
    const subscriptionTypeValues = this.subscriptionTypes[e.target.value];
    this.subscriptionForm.controls['ticketAmount'].setValue(subscriptionTypeValues.ticketAmount);
    this.subscriptionForm.controls['price'].setValue(subscriptionTypeValues.price);
  }

  addToSubscriptions() {
    this.subscriptions.push(this.subscriptionForm.value);
    this.toastr.showSuccess('', 'המנוי נוסף');
    this.subscriptionForm.controls['ownerName'].setValue('');
    this.subscriptionForm.controls['ownerPhone'].setValue('');
    this.subscriptionForm.controls['ownerMail'].setValue('');
  }

  onSubmit() {
    this.isLoading = true;
    console.log(this.subscriptionForm.value);
    this.subscriptions.push(this.subscriptionForm.value);
    this.adminService.addSubscriptions(this.subscriptions)
      .subscribe(
        val => {
          this.isLoading = false;
          this.newSubscriptions = val;
          this.displayType = 'succuss';
        },
        err => {
          this.toastr.showError('שגיאה', err);
          this.isLoading = false;
        }
      );
  }

  getNewSubscriptionsTotalPrice() {
    return this.newSubscriptions.reduce((sum, curr) => sum + curr.price, 0);
  }

  goToSubscriptions() {
    this.router.navigateByUrl('backoffice/subscriptions');
  }

}
