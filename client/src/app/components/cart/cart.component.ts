import { AdminService } from './../../core/services/admin.service';
import { Show } from './../../core/models/show.model';
import { Ticket } from './../../core/models/ticket.model';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import {StoreService, ToastyHelperService} from '../../core/services';
import {Acquisition, Subscription, Transaction} from '../../core/models';
import { hebrawWeekDay } from '../../../utils';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {DomSanitizer} from '@angular/platform-browser';

enum AdminPayState {
  specielPrice = '1',
  subscription = '2',
  noPrice = '3'
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  acquisitions: Acquisition[];
  tickets: Ticket[];
  subscription: Subscription;
  state  = 'cart';
  storeMode: string;
  hebrawWeekDay = hebrawWeekDay;
  creditTransaction: Transaction;
  tranzilaIframeUrl: any;
  isAdmin: boolean;
  adminPayState: string;
  isAdminSubscriptionPayToggled: boolean;
  subscriptionId: number;
  isTakanonToggled = false;

  payForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]),
    phone: new FormControl('', [Validators.required, Validators.pattern('0[0123456789]+'), Validators.maxLength(15)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    readTakanon: new FormControl(false, Validators.requiredTrue)
  });

  constructor(
    private storeService: StoreService,
    private toastr: ToastyHelperService,
    private router: Router,
    private domSanitizer: DomSanitizer,
    private adminService: AdminService
  ) {
    this.acquisitions = storeService.acquisitions;
    this.tickets = storeService.tickets;
  }

  deleteTicket(ticket: Ticket) {
    this.storeService.removeTicket(ticket);
  }

  getTicketsPrice() {
    return this.tickets.reduce((sum, t) => sum + t.show.price, 0);
  }

  onAdminSubscriptionPayClick() {
    this.isAdminSubscriptionPayToggled = !this.isAdminSubscriptionPayToggled;
  }

  removeTicketsByShow(show: Show) {
    this.storeService.removeTicketsByShow(show);
  }

  onPurches() {
    if (this.storeMode === 'subscription') {
      this.payWithSubscription();
    }
    if (this.storeMode === 'credit') {
      this.goToState('details');
    }
  }

  onPaySubmit() {
    console.log(this.payForm.value, this.storeMode);
    if (this.isAdmin && this.adminPayState === AdminPayState.specielPrice) {
      this.adminPayWithSpecielPrice();
      return;
    }
    if (this.storeMode === 'subscription') {
      this.payWithSubscription();
      return;
    }
    if (this.storeMode === 'credit') {
      this.payWithCredit();
      return;
    }

  }

  toggleTakanon() {
    this.isTakanonToggled = !this.isTakanonToggled;
  }

  payWithCredit() {
    this.goToState('loader');
    this.storeService.payWithCredit(this.payForm.value.name, this.payForm.value.phone, this.payForm.value.email)
      .subscribe(
        val => {
            this.creditTransaction = val.transaction;
            console.log(this.creditTransaction);
            // tslint:disable-next-line:max-line-length
            this.storeService.clearTickets();
            this.router.navigateByUrl(`/payment/${this.creditTransaction.id}`);
      }, err => {
        this.toastr.showError('', 'הירעה שגיאה');
        this.goToState('details');
      }
    );
  }

  payWithSubscription() {
    this.goToState('loader');
    this.storeService.payWithSubscription(this.payForm.value.name, this.payForm.value.phone, this.payForm.value.email)
      .subscribe(
        val => {
          this.storeService.clearTickets();
          this.acquisitions = [];
          this.goToState('details');
          this.storeService.clearSubscription();
          this.router.navigateByUrl('/success');
      }, err => {
        this.goToState('cart');
        this.toastr.showError('שגיאה', err.message);
      });
  }
  backToCart() {
    this.goToState('cart');
    this.adminPayState = undefined;
  }

  goToState(state: string) {
    this.state = state;
  }

  adminPayWithSpecielPrice() {
    this.goToState('loader');
    this.storeService.adminPayWithSpecielPrice(
      this.payForm.value.name,
      this.payForm.value.phone,
      this.payForm.value.email,
      this.payForm.value.specielPrice
    ).subscribe(val => {
      console.log(this.payForm.value);
      if (this.payForm.value.specielPrice === '0' || this.payForm.value.specielPrice === 0) {
        this.router.navigateByUrl('/success');
        return;
      }
      this.creditTransaction = val.transaction;
      console.log(this.creditTransaction);
      this.storeService.clearTickets();
      this.router.navigateByUrl(`/payment/${this.creditTransaction.id}`);
    }, err => {
      this.toastr.showError('', 'הירעה שגיאה');
      this.goToState('details');
    });
  }

  onSpecialPriceClick() {
    this.payForm.addControl('specielPrice', new FormControl('', [Validators.required]));
    this.adminPayState = AdminPayState.specielPrice;
    this.goToState('details');
  }

  onAdminSubscriptionClick() {
    this.goToState('loader');
    this.storeService.adminPayWithSubscription(this.subscriptionId)
      .subscribe(
        val => {
          this.storeService.clearTickets();
          this.acquisitions = [];
          this.goToState('cart');
          this.router.navigateByUrl('/success');
      }, err => {
        this.goToState('cart');
        this.toastr.showError('שגיאה', err.message);
      });
  }

  ngOnInit() {
    this.storeService.ticketsChange.subscribe(tickets => {
      console.log('tickets coming', tickets);
      this.tickets = tickets;
      console.log('this is the tickets now', this.tickets.length);
    });
    this.storeService.subscription.subscribe(
      val => this.subscription = val
    );
    this.storeService.mode.subscribe(
      val => this.storeMode = val
    );
    this.adminService.isAdmin.subscribe(val => this.isAdmin = val);
  }

}
