import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TicketsManagementComponent } from './tickets-management.component';

describe('TicketsManagementComponent', () => {
  let component: TicketsManagementComponent;
  let fixture: ComponentFixture<TicketsManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TicketsManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketsManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
