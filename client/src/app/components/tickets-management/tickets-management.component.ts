import { FormGroup, FormControl } from '@angular/forms';
import { ToastyHelperService } from './../../core/services/toasty-helper.service';
import { AdminService } from '../../core/services/admin.service';
import { Component, OnInit } from '@angular/core';
import { Ticket, Show } from '../../core/models';
import { ShowService } from '../../core/services';

@Component({
  selector: 'app-tickets-management',
  templateUrl: './tickets-management.component.html',
  styleUrls: ['./tickets-management.component.css']
})
export class TicketsManagementComponent implements OnInit {
  tickets: Ticket[] = [];
  selectedTickets: Ticket[] = [];
  shows: Show[] = [];

  isLoading: false;
  activeSearchRequests = 0;

  searchForm = new FormGroup({
    onlyReserved: new FormControl(false),
    selectedShow: new FormControl('כל ההקרנות'),
    filterText: new FormControl('')
  });

  constructor(
    private adminService: AdminService,
    private toastr: ToastyHelperService,
    private showService: ShowService
  ) { }

  selectTicket(ticket: Ticket) {
    this.selectedTickets.push(ticket);
  }
  onTicketClick(ticket: Ticket) {
    if (this.selectedTickets.includes(ticket)) {
      this.unselectTicket(ticket);
    } else {
      this.selectTicket(ticket);
    }
  }

  printTickets() {
    this.adminService.printTickets(this.selectedTickets)
      .subscribe(
        val => {this.selectedTickets = []; },
        err => this.toastr.showError('', 'שגיאה בהדפסת כרטיסים')
      );
  }

  onSelectAllTicketsClick() {
    if (this.selectedTickets.length === this.tickets.length) {
      this.selectedTickets = [];
    } else {
      this.selectedTickets = this.tickets.slice();
    }
  }

  unselectTicket(ticket: Ticket) {
    const index = this.selectedTickets.indexOf(ticket);
    this.selectedTickets.splice(index, 1);
  }

  dismissTicket(ticket: Ticket) {
    const isConfirmed = confirm('בטוח??');
    if (isConfirmed) {
      this.adminService.dismissTicket(ticket).subscribe(
        val => {
          this.toastr.showSuccess('', 'הכרטיס שוחרר');
          ticket.subscription = null;
        },
        err => this.toastr.showError('', 'שגיאה בשחרור כרטיס')
      );
    }
  }

  onSearceFormValueChange(val) {
    console.log(val, 'this is the value of the form');
    this.retriveTickets(val.onlyReserved, val.selectedShow ? val.selectedShow.id : undefined, val.filterText.trim());
  }

  retriveTickets(onlyReserved?, selectedShowId?, filterText?) {
    this.activeSearchRequests += 1;
    this.selectedTickets = [];
    this.adminService.retriveTickets(onlyReserved, selectedShowId, filterText).subscribe(
      val => {this.activeSearchRequests -= 1; },
      err => {
        this.activeSearchRequests -= 1;
        this.toastr.showError('שגיאה', 'שגיאה בטעינת כרטיסים');
      }
    );
  }

  ngOnInit() {
    this.searchForm.valueChanges.subscribe(val => this.onSearceFormValueChange(val));

    this.adminService.tickets.subscribe(
      val => this.tickets = val
    );

    this.retriveTickets();
    this.showService.getShows().subscribe(
      val => this.shows = val,
      err => this.toastr.showError('', 'שגיאה בטעינת סרטים')
    );
  }

}
