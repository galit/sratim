import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StoreService, ToastyHelperService } from '../../core/services';

@Component({
  selector: 'app-subscription',
  templateUrl: './subscription.component.html',
  styleUrls: ['./subscription.component.css']
})
export class SubscriptionComponent implements OnInit {
  ownerId: string;
  subscriptionId: string;
  nextUrl: string;

  constructor(
    private storeService: StoreService,
    private toastr: ToastyHelperService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  subscriptionLogin() {
    this.storeService.subscriptionLogin(this.ownerId, this.subscriptionId).subscribe(
      val => {
        this.toastr.showSuccess('המנוי נטען בהצלחה', '');
        this.router.navigateByUrl(this.nextUrl); },
      err => {
        this.toastr.showError('שגיאה', err.message);
        this.subscriptionId = '';
      }
    );
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      val => this.nextUrl = val.return || '/choose-seat'
    );
    this.storeService.isSubscriptionChecked = true;
  }

}
