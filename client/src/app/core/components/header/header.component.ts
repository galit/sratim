import { AdminService } from './../../services/admin.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private adminService: AdminService) { }
  isAdmin: boolean;
  ngOnInit() {
    this.adminService.isAdmin.subscribe(val => this.isAdmin = val);
  }

}
