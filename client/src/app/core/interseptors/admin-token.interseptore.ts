import { AdminService } from './../services/admin.service';
// tslint:disable-next-line:import-blacklist
import { Observable } from 'rxjs/Rx';
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';


@Injectable()
export class AdminInterceptor implements HttpInterceptor {
constructor(private adminService: AdminService) { }

intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq;

    // Clone the request to add the new header
    if (this.adminService.userJwt) {
        authReq = req.clone({ headers: req.headers.set('authentication', this.adminService.userJwt)});
    } else {
        authReq = req;
    }



    // send the newly created request
    return next.handle(authReq) as any;
    }
}
