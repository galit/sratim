import { DeviceGuard } from './services/device-guard';
import { AdminInterceptor } from './interseptors/admin-token.interseptore';
import { StorageService } from './services/storage.service';
import { AdminGuard } from './services/admin-guard';
import { AdminService } from './services/admin.service';
import { AuthService } from './services/auth.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ToastyModule } from 'ng2-toasty';
import { CookieModule } from 'ngx-cookie';
import { SlimLoadingBarModule } from 'ng2-slim-loading-bar';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import {
  ApiService,
  AppService,
  ToastyHelperService,
  RequestsService,
  StoreService,
  ShowService,
  WellcomeGaurd,
  StayInPaymentGuard
} from './services';
import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RouterModule } from '@angular/router';
import { SpinnerComponent } from './components/spinner/spinner.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    CookieModule.forRoot(),
    SlimLoadingBarModule.forRoot(),
    ToastyModule.forRoot(),
    RouterModule,
    SharedModule
  ],
  declarations: [HeaderComponent, FooterComponent, SpinnerComponent],
  providers: [
    ApiService,
    AuthService,
    AppService,
    ToastyHelperService,
    StoreService,
    ShowService,
    RequestsService,
    WellcomeGaurd,
    AdminService,
    AdminGuard,
    DeviceGuard,
    StayInPaymentGuard,
    StorageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AdminInterceptor,
      multi: true
    }
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SpinnerComponent,
    SlimLoadingBarModule,
    ToastyModule
  ]
})
export class CoreModule { }
