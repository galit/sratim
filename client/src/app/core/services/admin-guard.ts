import { AdminService } from './admin.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { StoreService } from './store.service';

@Injectable()
export class AdminGuard implements CanActivate {
    constructor(private adminService: AdminService, private router: Router) {}
    canActivate() {
        return this.adminService.isAdmin.map(val => {
            if (!val) {
                this.router.navigateByUrl('/backoffice/login');
            }
            return val;
        });
    }
}

