import { AdminService } from './admin.service';
import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { StoreService } from './store.service';

@Injectable()
export class WellcomeGaurd implements CanActivate {
    constructor(private storeService: StoreService, private router: Router, private adminService: AdminService) {}
    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.storeService.isSubscriptionChecked || this.adminService.isAdminSync()) {
            return true;
        }
        this.router.navigate(['/wellcome'], {queryParams: {return: state.url}});
    }
}

