import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import {Show, Ticket, Acquisition, Subscription} from '../models';
import { Observable } from 'rxjs';
import 'rxjs/add/observable/of';
import { StoreService } from './store.service';


@Injectable()
export class ShowService {
  currentSubscription: Subscription;

  constructor(private apiService: ApiService, private storeService: StoreService) {
    this.storeService.subscription.subscribe(val => this.currentSubscription = val);
  }

  getShowTickets(showId: number): Observable<Ticket[]> {
    return this.apiService.get(`/ticket/byShowId/${showId}`);
  }

  getShows(): Observable<Show[]> {
    return this.apiService.get('/show');
  }

  checkIfTicketsValid(tickets: Ticket[]) {
    console.log(this.currentSubscription);
    const ticketsIds = tickets.map(val => val.id);
    let body = {ticketsIds: ticketsIds};
    if (this.currentSubscription) {
      body['subscriptionIdJwt'] = this.currentSubscription.jwtId;
    }
    return this.apiService.post('/acquisition/reserve', body);
  }

}
