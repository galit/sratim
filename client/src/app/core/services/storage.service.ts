import { Injectable } from '@angular/core';
import {Storage} from '../models/storage.model';


const STORAGE = 'storage';
@Injectable()
export class StorageService {
    storage: Storage = {} as Storage;
    constructor() {
        if (sessionStorage.getItem(STORAGE)) {
            this.storage = JSON.parse(sessionStorage.getItem(STORAGE));
        }
    }

    updateStorage(attr: string, value: any) {
        this.storage[attr] = value;
        sessionStorage.setItem(STORAGE, JSON.stringify(this.storage));
    }

    isStorageExists() {
        return sessionStorage.getItem(STORAGE) ? true : false;
    }

    clearStorage() {
        sessionStorage.clear();
    }
}
