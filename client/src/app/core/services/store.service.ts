import { Show } from './../models/show.model';
import { Observable, ReplaySubject, BehaviorSubject } from 'rxjs';
import { ApiService } from './api.service';
import { Injectable } from '@angular/core';
import {Acquisition, Subscription, Transaction, Ticket} from '../models';
import 'rxjs/add/operator/share';
import { StorageService } from './storage.service';


@Injectable()
export class StoreService {
  acquisitions: Acquisition[] = [];
  tickets: Ticket[] = [];
  private _subscription: Subscription;
  private _subscription$: ReplaySubject<Subscription> = new ReplaySubject();
  public readonly subscription: Observable<Subscription> = this._subscription$.asObservable();
  private _mode: BehaviorSubject<string> = new BehaviorSubject('credit');
  public readonly mode: Observable<string> = this._mode.asObservable();
  private _isSubscriptionChecked = false;
  private _ticketsChange: BehaviorSubject<Ticket[]> = new BehaviorSubject(this.tickets);
  public readonly ticketsChange = this._ticketsChange.asObservable();

  constructor(private apiService: ApiService, private storageService: StorageService) {
    if (this.storageService.isStorageExists()) {
      this._isSubscriptionChecked = this.storageService.storage.isSubscriptionChacked;
      this._subscription = this.storageService.storage.subscription;
      if (this._subscription) {
        this._subscription$.next(this._subscription);
      }
      if (this.storageService.storage.storeMode) {
        this._mode.next(this.storageService.storage.storeMode);
      }
      if (this.storageService.storage.selectedTickets) {
        this.tickets = this.storageService.storage.selectedTickets;
        this._ticketsChange.next(this.tickets);
      }
    }
  }

  set isSubscriptionChecked(state: boolean) {
    console.log('i am here', state);
    this.storageService.updateStorage('isSubscriptionChacked', state);
    this._isSubscriptionChecked = state;
  }

  get isSubscriptionChecked() {
    return this._isSubscriptionChecked;
  }

  addAcquisition(acquisition: Acquisition) {
    this.acquisitions.push(acquisition);
    return true;
  }

  addTickets(tickets: Ticket[]): void {
    console.log('hello', tickets);
    this.tickets = this.tickets.concat(tickets);
    console.log(this.tickets);
    this._ticketsChange.next(this.tickets);
    this.storageService.updateStorage('selectedTickets', this.tickets);
  }

  removeTicketsByShow(show: Show): void {
    this.tickets = this.tickets.filter(t => t.show.id !== show.id);
    this._ticketsChange.next(this.tickets);
    this.storageService.updateStorage('selectedTickets', this.tickets);
  }

  removeTicket(ticket: Ticket): void {
    this.tickets = this.tickets.filter(t => t.id !== ticket.id);
    this._ticketsChange.next(this.tickets);
    this.storageService.updateStorage('selectedTickets', this.tickets);
  }

  removeAcquisition(acquisition: Acquisition) {
    const index = this.acquisitions.indexOf(acquisition);
    this.acquisitions.splice(index, 1);
  }
  clearAcquisitions() {
    this.acquisitions = [];
  }

  clearTickets() {
    this.tickets = [];
    this._ticketsChange.next(this.tickets);
    this.storageService.updateStorage('selectedTickets', this.tickets);
  }

  subscriptionLogin(ownerId: string, subscriptionId: string): Observable<Subscription> {
    const obs = this.apiService.post('/subscription/subscription-login', {
      ownerId: ownerId,
      subscriptionId: subscriptionId
    }).share();

    obs.subscribe(val => {
      this.storageService.updateStorage('subscription', val);
      this.storageService.updateStorage('storeMode', 'subscription');
      this._subscription$.next(val);
      this._subscription = val;
      this._mode.next('subscription');
    });

    return obs;
  }

  payWithSubscription(name: string, phone: string, email: string): Observable<any> {
    const postBody = {
      subscriptionIdJwt: this._subscription.jwtId,
      ticketsIds: this.tickets.map(val => val.id)
    };
    return  this.apiService.post('/acquisition/subscription', postBody);

  }

  adminPayWithSpecielPrice(name: string, phone: string, email: string, specielPrice: number): Observable<any> {
    const postBody = {
      specielPrice: specielPrice,
      name: name,
      phone: phone,
      email: email,
      ticketsIds: this.tickets.map(val => val.id)
    };
    return this.apiService.post('/admin/speciel-price-pay', postBody);
  }

  adminPayWithSubscription(subscriptionId): Observable<any> {
    const postBody = {
      subscriptionId: subscriptionId,
      ticketsIds: this.tickets.map(val => val.id)
    };
    return  this.apiService.post('/admin/subscription-pay', postBody);
  }

  getSubscriptionById(subscriptionId: number): Observable<Subscription> {
    return this.apiService.get(`/subscription/${subscriptionId}`);
  }

  payWithCredit(name: string, phone: string, email: string): Observable<{transaction: Transaction}> {
    const postBody = {
      name: name,
      phone: phone,
      email: email,
      ticketsIds: this.tickets.map(val => val.id)
    };
    return  this.apiService.post('/acquisition/credit', postBody);

  }

  clearSubscription() {
    this._subscription = undefined;
    this._subscription$.next(this._subscription);
    this.isSubscriptionChecked = false;
    this._mode.next('credit');
    this.storageService.clearStorage();
  }
}
