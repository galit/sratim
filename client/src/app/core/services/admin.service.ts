import { StorageService } from './storage.service';
import { HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Subscription, Ticket } from '../models';

@Injectable()
export class AdminService {
    private _subscriptions: BehaviorSubject<Subscription[]> = new BehaviorSubject([]);
    public readonly subscriptions: Observable<Subscription[]> = this._subscriptions.asObservable();

    private _tickets: BehaviorSubject<Ticket[]> = new BehaviorSubject([]);
    public readonly tickets: Observable<Ticket[]> = this._tickets.asObservable();

    private _isAdmin: BehaviorSubject<boolean> = new BehaviorSubject(false);
    public readonly isAdmin: Observable<boolean> = this._isAdmin.asObservable();

    userJwt: string;

    constructor(private apiService: ApiService, private storageService: StorageService) {
        if (this.storageService.isStorageExists()) {
            this._isAdmin.next(this.storageService.storage.isAdmin);
            this.userJwt = this.storageService.storage.userJwt;
        }
    }

    retriveSubscriptions() {
        const obs = this.apiService.get('/admin/get-subscriptions').share();

        obs.subscribe(val => this._subscriptions.next(val));

        return obs;
    }

    retriveTickets(onlyReserved: any = false, showId?: number, searchString?: string) {
        onlyReserved = onlyReserved ? 1 : 0;
        let params = new HttpParams().set('onlyReserved', onlyReserved);
        if (showId) {
            params = params.append('showId', showId.toString());
        }
        if (searchString) {
            params = params.append('searchString', searchString);
        }

        const obs = this.apiService.get('/admin/get-tickets', params).share();

        obs.subscribe(val => this._tickets.next(val));

        return obs;
    }

    addSubscriptions(subscriptions: Subscription[]) {
        return this.apiService.post('/admin/add-subscription', {subscriptions: subscriptions});
    }

    printTickets(tickets: Ticket[]) {
        const ticketIds = tickets.map(val => val.id);
        const obs = this.apiService.post('/print-tickets/by-ids', {ticketIds}).share();
        obs.subscribe(
                val => {
                    const printWindow = window.open('', 'print', 'height=1000,width=1000');
                    printWindow.document.write(val.pritnPage);
                },
                err => console.log(err)
            );
        return obs;
    }

    dismissTicket(ticket: Ticket) {
        return this.apiService.post('/admin/dismiss-ticket', {ticketId: ticket.id});
    }

    adminLogin(username: string, password: string): Observable<Subscription> {
        const obs = this.apiService.post('/admin/login', {
          username: username,
          password: password
        }).share();

        obs.subscribe(val => {
          this.storageService.updateStorage('isAdmin', true);
          this.storageService.updateStorage('userJwt', val.userJwt);
          this._isAdmin.next(true);
          this.userJwt = val.userJwt;
        });

        return obs;
      }

    isAdminSync() {
        return this._isAdmin.value;
    }

}
