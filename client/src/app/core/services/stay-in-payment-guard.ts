import { PaymentComponent } from './../../components/payment/payment.component';
import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';


@Injectable()
export class StayInPaymentGuard implements CanDeactivate<PaymentComponent> {
   canDeactivate() {
        return confirm('בטוח שברצונך לצאת מהעמוד?');
    }

}