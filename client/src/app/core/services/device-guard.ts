import { ApiService } from './api.service';
import { environment } from './../../../environments/environment';
import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { DeviceDetectorService } from 'ngx-device-detector';


@Injectable()
export class DeviceGuard implements CanActivate {
    constructor(
        private deviceDetectorService: DeviceDetectorService,
        private router: Router,
        private apiService: ApiService
    ) {}

    canActivate() {
        const browserConfig = environment.browsers.find(val => val.name === this.deviceDetectorService.browser);
        if (browserConfig) {
            if (browserConfig.minVersion > parseInt(this.deviceDetectorService.browser_version, 10)) {
                this.apiService.post('/not-suppurted-browser').subscribe();
                this.router.navigateByUrl('/not-suppurted');
            }
        }
        return true;
    }
}
