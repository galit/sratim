export interface Seat {
  id: number;
  section: number;
  row: number;
  seatId: number;
}
