import {Show} from './show.model';
import {Ticket} from './ticket.model';

export interface Acquisition {
  show: Show;
  tickets: Ticket[];
}
