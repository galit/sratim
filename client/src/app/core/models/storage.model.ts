import { Subscription } from './subscription.model';
import { Ticket } from './ticket.model';


export interface Storage {
    isAdmin: boolean;
    userJwt: string;
    isSubscriptionChacked: boolean;
    selectedTickets: Ticket[];
    subscription: Subscription;
    storeMode: string;
}
