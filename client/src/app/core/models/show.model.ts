export interface Show {
  id: number;
  price: number;
  name: string;
  dateTime: Date;
}
