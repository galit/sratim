export interface Subscription {
    id?: number;
    isConfirmed?: boolean;
    ticketAmount: number;
    usage: number;
    ownerName: string;
    ownerPhone: string;
    ownerId: string;
    price: number;
    ownerMail: string;
    jwtId?: string;
}
