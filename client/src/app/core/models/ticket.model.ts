import {Seat} from './seat.model';
import {Show} from './show.model';
import { Subscription } from './subscription.model';

export interface Ticket {
  id: number;
  isChosen: boolean;
  isTaken: boolean;
  isPrinted?: boolean;
  seat: Seat;
  show: Show;
  subscription: Subscription;
}
