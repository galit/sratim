export interface Transaction {
    id: number;
    price: number;
}
