import { Subscription } from './../core/models';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'filterSubscriptions'})
export class FilterSubscriptionsPipe implements PipeTransform {
    transform(subscriptions: Subscription[], filterBy: string) {
        if (!filterBy) {
            return subscriptions;
        }
        return subscriptions.filter(subscription => {
            // handle nulls
            return subscription.ownerName.includes(filterBy) ||
                subscription.ownerId.includes(filterBy) ||
                subscription.ownerMail.includes(filterBy) ||
                subscription.ownerPhone.includes(filterBy) ||
                subscription.id.toString().includes(filterBy);
        });
    }
}
