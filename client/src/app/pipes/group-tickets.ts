import { Ticket, Acquisition } from './../core/models';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'groupTickets'})
export class GroupTicketsPipe implements PipeTransform {
    transform(tickets: Ticket[]) {
        console.log('this is the tickets from the pipe', tickets);
        const temp = {};
        tickets.forEach(ticket => {
            if (temp[ticket.show.id] === undefined) {
                temp[ticket.show.id] = [];
            }
            temp[ticket.show.id].push(ticket);
        });
        // console.log('this is the values', Object.values(temp));
        // return Object.values(temp);
        return Object.keys(temp).map(val => temp[val]);
    }
}
