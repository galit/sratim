const HEBRAW_DAYS = {
  0: 'ראשון',
  1: 'שני',
  2: 'שלישי',
  3: 'רביעי',
  4: 'חמישי',
  5: 'שישי',
  6: 'שבת'
};

export function hebrawWeekDay(date: string) {
  const ndate = new Date(date);
  return HEBRAW_DAYS[ndate.getDay()];
}

