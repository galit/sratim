export const environment = {
  production: true,
  api_url: 'https://aravatickets.com',
  masof: 'arava',
  browsers: [
    {
      name: 'firefox',
      minVersion: 51
    },
    {
      name: 'safari',
      minVersion: 9
    },
    {
      name: 'ie',
      minVersion: 11
    },
    {
      name: 'chrome',
      minVersion: 36
    }
  ],
  socialLogin: {
    'facebook': 'YOUR-APP-ID',
    'google': 'YOUR-APP-ID'
  }
};
