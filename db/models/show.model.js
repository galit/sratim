const Sequelize = require('sequelize');

class Show extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                name: {
                    type: DataTypes.STRING(200),
                    allowNull: false
                },
                price: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                dateTime: {
                    type: DataTypes.DATE,
                    allowNull: false
                }
            },
            {
                tableName: 'Show',
                charset: 'utf8',
                collate: 'utf8_unicode_ci',
                sequelize
            }
        )
    }

    static associate(models) {
        this.hasMany(models.Ticket, {as: 'tickets', foreignKey: 'showId'})
    }
}

module.exports = Show;