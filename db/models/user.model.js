const Sequelize = require('sequelize');

class User extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                username: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                password: {
                    type: DataTypes.STRING,
                    allowNull: false
                }
            },
            {
                tableName: 'User',
                timestamps: false,
                sequelize
            }
        )
    }

    static associate(models) {

    }
}

module.exports = User;