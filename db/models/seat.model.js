const Sequelize = require('sequelize');

class Seat extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                section: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                row: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                seatId: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                }
            },
            {
                tableName: 'Seat',
                sequelize
            }
        )
    }

    static associate(models) {}
}

module.exports = Seat;