const Sequelize = require('sequelize');

class Transaction extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                price: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                isConfirmed: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                },
                isNotified: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                },
                isFailed: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                }
            },
            {
                tableName: 'Transaction',
                sequelize
            }
        )
    }

    static associate(models) {}
}

module.exports = Transaction;