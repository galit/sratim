const Sequelize = require('sequelize');

class Acquisition extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                uuid: {
                    type: DataTypes.UUID,
                    defaultValue: DataTypes.UUIDV4,
                    allowNull: false,
                    unique: true
                },
                name: {
                    type: DataTypes.STRING(200),
                    allowNull: false
                },
                phone: {
                    type: DataTypes.STRING(200),
                    allowNull: false
                },
                email: {
                    type: DataTypes.STRING(200),
                    allowNull: false
                }
            },
            {
                tableName: 'Acquisition',
                charset: 'utf8',
                collate: 'utf8_unicode_ci',
                sequelize
            }
        )
    }

    static associate(models) {
        this.hasMany(models.Ticket, {as: 'tickets', foreignKey: 'acquisitionId'});
        this.belongsTo(models.Subscription, {as: 'subscription', foreignKey: 'subscriptionId'})
        this.belongsTo(models.Transaction, {as: 'transaction'})
    }
}

module.exports = Acquisition;