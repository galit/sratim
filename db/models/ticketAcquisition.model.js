const Sequelize = require('sequelize');

class TicketAcquisition extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                AcquisitionId: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                TicketId: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                }
            },
            {
                tableName: 'TicketAcquisition',
                sequelize
            }
        )
    }

}

module.exports = TicketAcquisition;