const Show = require('./show.model');
const Ticket = require('./ticket.model');
const Seat = require('./seat.model');
const Acquisition = require('./acquisition.model');
const Subscription = require('./subscription.model');
const Transaction = require('./transaction.model');
const User = require('./user.model');

module.exports = {
    Transaction,
    Subscription,
    Acquisition,
    Show,
    Seat,
    Ticket,
    User
}