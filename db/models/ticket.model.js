const Sequelize = require('sequelize');

class Ticket extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                uuid: {
                    type: DataTypes.UUID,
                    defaultValue: DataTypes.UUIDV4,
                    allowNull: false,
                    unique: true
                },
                lastReserved: {
                    type: DataTypes.DATE,
                    allowNull: true
                },
                isPrinted: {
                    type: DataTypes.BOOLEAN,
                    defaultValue: false
                },
                isArrived: {
                    type: DataTypes.BOOLEAN,
                    defaultValue: false
                }
            },
            {
                tableName: 'Ticket',
                sequelize
            }
        )
    }

    static associate(models) {
        this.belongsTo(models.Seat, {as: 'seat'})
        this.belongsTo(models.Show, {as: 'show'})
        this.belongsTo(models.Subscription, {as: 'subscription', foreignKey: 'subscriptionId'})
        this.belongsTo(models.Acquisition, {as: 'acquisition'})
    }
}

module.exports = Ticket;