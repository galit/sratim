const Sequelize = require('sequelize');

class Subscription extends Sequelize.Model {
    static init(sequelize, DataTypes) {
        return super.init(
            {
                id: {
                    type: DataTypes.INTEGER,
                    primaryKey: true,
                    autoIncrement: true
                },
                uuid: {
                    type: DataTypes.UUID,
                    defaultValue: DataTypes.UUIDV4,
                    allowNull: false,
                    unique: true
                },
                type: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                secret: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                ticketAmount: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                ownerName: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                ownerPhone: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                ownerMail: {
                    type: DataTypes.STRING,
                    allowNull: false
                },
                ownerId: {
                    type: DataTypes.STRING
                },
                price: {
                    type: DataTypes.INTEGER,
                    allowNull: false
                },
                isConfirmed: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                },
                isNotified: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                },
                isFailed: {
                    type: DataTypes.BOOLEAN,
                    allowNull: true
                },
                myId: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                creditNumber: {
                    type: DataTypes.STRING,
                    allowNull: true
                },
                creditSum: {
                    type: DataTypes.INTEGER,
                    allowNull: true
                },
                creditTk: {
                    type: DataTypes.STRING,
                    allowNull: true
                }
            },
            {
                tableName: 'Subscription',
                // charset: 'utf8',
                // collate: 'utf8_unicode_ci',
                sequelize
            }
        )
    }

    static associate(models) {
        this.hasMany(models.Ticket, {as: 'tickets', foreignKey: 'subscriptionId'})
    }
}

module.exports = Subscription;