const dbModels = require('../../db').models;
const db = require('../../db').sequelize;
// const argv = require('yargs').argv;

const name = 'ציפורים נודדות';
const price = 45;
const dateTime = new Date(2018,11,18,17,15);

async function insertShow(name, price, dateTime) {
    const t = await db.transaction();
    try{     
        const seats = await dbModels.Seat.findAll()
        const newShow = await dbModels.Show.create({name: name, price: price, dateTime: dateTime}, {transaction: t})
        for (const seat of seats) {
            await dbModels.Ticket.create({showId: newShow.id, seatId: seat.id}, {transaction: t})
        }
        await t.commit()
    } catch(err) {
        await t.rollback()
        throw(err)
    }
}

// insertShow('עוד סרט', 74, new Date(2018,10,5,20,30))
// insertShow('סרט שני', 50, new Date(2018,10,6,22))
insertShow(name, price, dateTime).then(val => console.log('success')).catch(err => console.log(err))

