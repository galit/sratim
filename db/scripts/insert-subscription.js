const dbModels = require('../../db').models;

dbModels.Subscription.create({
    id: 64531,
    secret: '1234',
    ticketAmount: 5,
    ownerName: 'אייל שיראי',
    ownerMail: 'eshiray@zahav.net.il',
    price: 250,
    ownerPhone: '0508631092',
    type: 'subscription'
})
.then(val => console.log('subscription inserted'))