const Sequelize = require('sequelize');
const sequelize = new Sequelize('dev', 'aravadb', 'Odelia6600!', {
    host: "aravadv.database.windows.net",
    dialect: 'mssql',
    operatorsAliases: false,
    dialectOptions: {
        encrypt: true
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    },
}); 
const models = require('./models');

Object.keys(models).forEach(modelKey => {
    models[modelKey] = models[modelKey].init(sequelize, Sequelize);
})

Object.values(models)
    .filter(model => typeof model.associate === 'function')
    .forEach(model => model.associate(models));

module.exports = {
    models: models,
    sequelize: sequelize
}
