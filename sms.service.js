const axios = require('axios');
const config = require('config');

function sendMessage(phone, message, reference = 0) {
    return axios.post(config.sms.url, {
        sms: {
            account: {
                id: config.sms.username,
                password: config.sms.password
            },
            attributes: {
                reference: reference,
                replyPath: config.sms.source
            },
            schedule: {
                relative: "0"
            },
            targets: {
                cellphone: [
                    {
                        "@reference": "0",
                        "#text": phone
                    }
                ]
            },
            data: message
        }
    })
}

function sendConfirmation(subscription) {
    const messageText = `הזמנת הכרטיסים שלך באתר התקבלה בהצלחה, הכרטיסים יאספו מקופת הפסטיבל. מספר הזמנה : ${subscription.id}`
    const phoneNumber = subscription.ownerPhone;
    return sendMessage(phoneNumber, messageText);
}

function sendSubscriptionConfiramtion(subscription) {
    const messageText = `מנוי הוקם עבורך לפסטיבל סרטים בערבה, מספר המנוי ${subscription.id}, לאתר הפסטיבל: https://www.aravaff.co.il`
    const phoneNumber = subscription.ownerPhone;
    return sendMessage(phoneNumber, messageText);
}

module.exports = {
    sendMessage,
    sendConfirmation,
    sendSubscriptionConfiramtion
}