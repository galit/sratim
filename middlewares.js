const config = require('config');
const jwt = require('jsonwebtoken');
const httpError = require('http-errors');


function assertAdmin(req, res, next) {
    let userId;
    try {
        userId = jwt.verify(req.get('authentication'), config.jwtSecret)
    } catch(err) {
        next(httpError(404, 'not autherised'))
    }
    
    console.log(userId, 'this is it!!!!');
    next()
}

module.exports = {
    assertAdmin
}