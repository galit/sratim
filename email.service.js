const config = require('config');
const sgMail = require('@sendgrid/mail');
const nunjucks = require('nunjucks');

sgMail.setApiKey(config.sendgridSecret);


function sendConfirmation(subscription) {
    const msg = {
        to: subscription.ownerMail,
        from: 'aravaff@aravatickets.com',
        subject: 'סרטים בערבה - אישור הזמנה',
        text: `הזמנת הכרטיסים שלך באתר התקבלה בהצלחה, הכרטיסים יאספו מקופת הפסטיבל. מספר הזמנה : ${subscription.id}`,
        html: nunjucks.render('./tamplates/confirmation-mail.html', {subscription: subscription}),
      };
    return sgMail.send(msg)
}

module.exports = {
    sendConfirmation
}
